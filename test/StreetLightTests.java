/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import br.puc.rio.les.testapp.streetlight.subscriber.StreetLightGlobalTest;
import br.puc.rio.les.testapp.streetlight.subscriber.StreetLightSubscriber;
import br.pucrio.les.mas.subscriber.framework.statemachine.StateMachine;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author nathalianascimento
 */
public class StreetLightTests {
    StreetLightSubscriber localTest = new StreetLightSubscriber();
    public StreetLightTests() {
         localTest.execute();
         sleep(200); //tem q dar tempo de startur o framework e aplicaca
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
     @Test
     public void switchLight03ONStateMachine() {
         
        
        
        StateMachine st03 = localTest.getStateMachineList().get(0);
        assertEquals(st03.getLast().name(),st03.getCurrent().name());
        
        

     }
     
     @Test
     public void switchLight10ONStateMachine() {
         StateMachine st10 = localTest.getStateMachineList().get(1);
         assertEquals(st10.getLast().name(),st10.getCurrent().name());

     }
     
     private void sleep(int seconds) {

        try {
            TimeUnit.SECONDS.sleep(seconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
     
     
}
