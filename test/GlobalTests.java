/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import br.puc.rio.les.testapp.streetlight.subscriber.StreetLightGlobalTest;
import br.pucrio.les.mas.subscriber.framework.statemachine.StateMachine;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author nathalianascimento
 */
public class GlobalTests {
    
    public GlobalTests() {
         
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
     @Test
     public void globalStateMachine() {
         StreetLightGlobalTest globalTest = new StreetLightGlobalTest();
      
         globalTest.execute();
         
         // Wait for your code to complete
        sleep(200); //tem q dar tempo de startur o framework e aplicacao
        List<StateMachine> list = globalTest.getStateMachineList();

        for(StateMachine machine: list){
             // Test the results
            assertEquals(machine.getLast().name(),machine.getCurrent().name());
        }

     }
     
     private void sleep(int seconds) {

        try {
            TimeUnit.SECONDS.sleep(seconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
     
     
}
