/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.puc.rio.les.testapp.booksale.subscriber.performance;

import br.pucrio.les.mas.subscriber.framework.LogValues;



/**
 *
 * @author nathalianascimento
 */
public class LogPerformanceValue implements LogValues{
    
   public enum Action implements LogValues.Action {
        create,connectToSystem,searchBook,receiveListFromDF,isDestroyed,askPrice,
        receivedBudget,selectBestPrice, askBuy, receivedSaleConfirmation,receivedBook;
    }
    public enum TypeLog implements LogValues.TypeLog{
        INFO, WARNING, ERROR;
    }
  /*  public enum Resource implements LogValues.Resource{
         book,yellowpage,agent,none;
    }
    public enum MethodName implements LogValues.MethodName{
        initAgent,setup,takeDown,buyBook,done,getBookInPostOffice;
    }*/
    public enum AgentType implements LogValues.AgentType{
        CLIENT,SELLER;
    }
   
}
