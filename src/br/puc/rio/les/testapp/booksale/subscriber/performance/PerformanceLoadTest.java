/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.puc.rio.les.testapp.booksale.subscriber.performance;

import br.pucrio.les.mas.subscriber.framework.performance.GuiPerformance;
import br.pucrio.les.mas.subscriber.framework.performance.MessageObservable;
import br.pucrio.les.mas.subscriber.framework.performance.SerieLoadGraph;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import br.pucrio.les.mas.subscriber.framework.BindingKey;
import br.pucrio.les.mas.subscriber.framework.LogContext;
import br.pucrio.les.mas.subscriber.framework.RabbitMQConsumer;

/**
 *
 * @author nathalianascimento
 */
    
public class PerformanceLoadTest extends RabbitMQConsumer{
    private int numTotalAgents = 0;
    private int numTotalSellers = 0;
    private int numTotalClients = 0;
    
    
    boolean process = true;
    
   // private final int numExecutions = 0;

    private static PerformanceLoadTest instance;
    String messageInfo = "";
    SerieLoadGraph series;
    List<String> listActions;
    private PerformanceLoadTest(){
        listActions = new ArrayList<>();
       // listActions.add(LogSystem.Action.create.name());
        //listActions.add(LogSystem.Action.connectToSystem.name());
        listActions.add(LogPerformanceValue.Action.searchBook.name());
        listActions.add(LogPerformanceValue.Action.receiveListFromDF.name());
        listActions.add(LogPerformanceValue.Action.selectBestPrice.name());
        listActions.add(LogPerformanceValue.Action.askBuy.name());
        listActions.add(LogPerformanceValue.Action.receivedSaleConfirmation.name());
       // listActions.add(LogSystem.Action.receivedBook.name());
        
        String firstAction = LogPerformanceValue.Action.searchBook.name();
        String unsteadyAction = LogPerformanceValue.Action.selectBestPrice.name();
        series = new SerieLoadGraph(listActions,firstAction,unsteadyAction);
    }
    public static PerformanceLoadTest getInstance() {

        if (instance == null) {
            inicializaInstancia();
        }
        return instance;
    }

    private static synchronized void inicializaInstancia() {
        if (instance == null) {
            instance = new PerformanceLoadTest();
        }
    }
    
    private void initiateNewGraphGenerator(){
        numTotalAgents = 0;
        numTotalSellers = 0;
        numTotalClients = 0;
        series.initializeMap();      
    }
   // public void addAgent(AgentLog agent){
     //   if()
   // }
    

    public String getMessageInfo() {
        return messageInfo;
    }

    public void setMessageInfo(String messageInfo) {
        this.messageInfo = messageInfo;
    }

    @Override
    protected void processData(LogContext log) {
        this.calculateNumberAgents(log);
        
        if(process){
        if(log.getAgentType().equals(LogPerformanceValue.AgentType.CLIENT.name())){
            if(this.listActions.contains(log.getAction())){
               double time = calculateTimeBehavior(log);
               series.setActionTime(log.getAction(), time);
            }
            
        }
        
        if(this.listActions.size() == series.getNumActionsExecuted() || series.numTimesUnsteadyAction>2){
            //double[][] serie = series.createSeries();
            series.setNumResponseAgents(this.numTotalSellers);
            
            MessageObservable.getInstance().changeData(series);
            this.initiateNewGraphGenerator();
            process = false;
        }
        }
    }
    
    protected void calculateNumberAgents(LogContext log){
        if(log.getAction().equals(LogPerformanceValue.Action.connectToSystem.name())){
            if(log.getTypeLog().equals(LogPerformanceValue.TypeLog.INFO.name())){
                if(log.getAgentType().equals(LogPerformanceValue.AgentType.CLIENT.name())){
                    this.numTotalClients++;
                    this.numTotalAgents++;
                }
                else{
                    this.numTotalSellers++;
                    this.numTotalAgents++;
                }
            }
            process = true;
        }
        if(log.getAction().equals(LogPerformanceValue.Action.isDestroyed.name())){
                if(log.getAgentType().equals(LogPerformanceValue.AgentType.CLIENT.name())){
                    this.numTotalClients--;
                    this.numTotalAgents--;
                }
                else{
                    this.numTotalSellers--;
                    this.numTotalAgents--;
                }
                
                if(this.numTotalAgents<0){
                    numTotalAgents = 0;
                    numTotalSellers = 0;
                    numTotalClients = 0;
                }
        }
        
    }
    
    protected double calculateTimeBehavior(LogContext log){
        
        double timeBehavior = 0;
        //convert to millisecond
        double hours = Integer.parseInt(log.getHour()) * 3600*1000;
        double minutes = Integer.parseInt(log.getMinute()) * 60 *1000;
        double seconds = Integer.parseInt(log.getSecond()) * 1000;
        double milliseconds = Integer.parseInt(log.getMillisecond());
        timeBehavior = hours + minutes + seconds + milliseconds;
        return timeBehavior;
    }

    @Override
    public String getBindingKey() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void testGui() {
       
        String[] actionsInArray = new String[listActions.size()];
        for (int i = 0; i < listActions.size(); i++) {
             actionsInArray[i] = listActions.get(i);
        }
        GuiPerformance.execute(actionsInArray);
    }

    @Override
    public String[] getListBindingKey() {
        BindingKey bd = new BindingKey();
        String[] listKey = new String[4];
        listKey[0] = bd.createBindingKey(LogPerformanceValue.Action.connectToSystem, LogPerformanceValue.TypeLog.INFO);
        listKey[1] = bd.createBindingKey(LogPerformanceValue.Action.isDestroyed);
        listKey[2] = bd.createBindingKey(LogPerformanceValue.AgentType.CLIENT, LogPerformanceValue.TypeLog.INFO);
        listKey[3] = bd.createBindingKey(LogPerformanceValue.Action.selectBestPrice, LogPerformanceValue.TypeLog.WARNING);
        return listKey; 
    }

    @Override
    public void createStateMachines() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
