/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.puc.rio.les.testapp.streetlight.subscriber;

import br.pucrio.les.mas.subscriber.framework.functional.MessageObservable;
import br.pucrio.les.mas.subscriber.framework.functional.IntegrationTestDemo;
import br.pucrio.les.mas.subscriber.framework.BindingKey;
import br.pucrio.les.mas.subscriber.framework.LogContext;
import br.pucrio.les.mas.subscriber.framework.RabbitMQConsumer;

/**
 *
 * @author nathalianascimento
 */
public class StreetLightSubscriberEnergy extends RabbitMQConsumer{

    @Override
    protected void processData(LogContext log) {
        //log.printLogValues();
         MessageObservable.getInstance().changeData(log);
    }

    @Override
    public String getBindingKey() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String[] getListBindingKey() {
        BindingKey bd = new BindingKey();
        String[] listKey = new String[3];
        listKey[0] = bd.createBindingKey(LogStreetValues.Action.setOutputValue);
     //  // listKey[1] = bd.createBindingKey(LogStreetValues.Action.calculateEnergy);
        listKey[1] = bd.createBindingKey(LogStreetValues.Action.readInputValue);
        
        listKey[2] = bd.createBindingKey(LogStreetValues.Action.finishSimulation);
        
        
      //  listKey[2] = bd.createBindingKey(LogStreetValues.Action.changePeoplePosition);
       // listKey[3] = bd.createBindingKey(LogStreetValues.Action.calculateEdgeLight);
        //listKey[3] = bd.createBindingKey(LogStreetValues.Action.receiveWirelessData);
       // listKey[2] = bd.createBindingKey(LogStreetValues.Action.sendMsg);
//        listKey[1] = bd.createBindingKey(LogStreetValues.Action.connectToSystem);
//        listKey[2] = bd.createBindingKey(LogStreetValues.AgentType.CLIENT, LogStreetValues.Action.searchBook);
//        listKey[3] = bd.createBindingKey(LogStreetValues.AgentType.CLIENT, LogStreetValues.Action.selectBestPrice);
//        listKey[4] = bd.createBindingKey(LogStreetValues.AgentType.CLIENT, LogStreetValues.Action.askBuy);
//        listKey[5] = bd.createBindingKey(LogStreetValues.AgentType.CLIENT, LogStreetValues.Action.receivedSaleConfirmation);
//        listKey[6] = bd.createBindingKey(LogStreetValues.AgentType.CLIENT, LogStreetValues.Action.receivedBook);
//        listKey[7] = bd.createBindingKey(LogStreetValues.AgentType.SELLER, LogStreetValues.Action.registerService);
//        listKey[8] = bd.createBindingKey(LogStreetValues.AgentType.SELLER, LogStreetValues.Action.addBook);
//        listKey[9] = bd.createBindingKey(LogStreetValues.AgentType.SELLER, LogStreetValues.Action.receivedBuy);
//        listKey[10] = bd.createBindingKey(LogStreetValues.AgentType.SELLER, LogStreetValues.Action.removeBook);
        return listKey;
    }

    @Override
    public void testGui() {
        new IntegrationTestDemo().showGui();
    }

    @Override
    public void createStateMachines() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
