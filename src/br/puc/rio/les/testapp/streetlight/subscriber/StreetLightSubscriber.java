/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.puc.rio.les.testapp.streetlight.subscriber;

import br.pucrio.les.mas.subscriber.framework.functional.MessageObservable;
import br.pucrio.les.mas.subscriber.framework.functional.IntegrationTestDemo;
import br.pucrio.les.mas.subscriber.framework.BindingKey;
import br.pucrio.les.mas.subscriber.framework.LogContext;
import br.pucrio.les.mas.subscriber.framework.RabbitMQConsumer;
import br.pucrio.les.mas.subscriber.framework.statemachine.StateMachine;
import br.pucrio.les.mas.subscriber.framework.statemachine.StateValues;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nathalianascimento
 */
public class StreetLightSubscriber extends RabbitMQConsumer{


    List<StateMachine> tests = new ArrayList<>();
    @Override
    protected void processData(LogContext log) {
        //log.printLogValues();
         MessageObservable.getInstance().changeData(log);
         this.processStateMachines(log);
    }

    @Override
    public String getBindingKey() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String[] getListBindingKey() {
        BindingKey bd = new BindingKey();
        String[] listKey = new String[5];
        //According to the activity diagram of street lights
        listKey[0] = bd.createBindingKey(LogStreetValues.Action.receiveNeuralNetworkCommand);
        listKey[1] = bd.createBindingKey(LogStreetValues.Action.switchLightOFF);
        listKey[2] = bd.createBindingKey(LogStreetValues.Action.switchLightON);
        listKey[3] = bd.createBindingKey(LogStreetValues.Action.detectLight);
        listKey[4] = bd.createBindingKey(LogStreetValues.Action.finishSimulation);
        
        return listKey;
    }
    public enum State implements StateValues.State {
        first,processNeuralNetwork,switchLightON,switchLightOFF,detectLight,
        last,error;
    }
    @Override
    public void createStateMachines() {
        StateMachine local01 = new StateMachine("SwitchLightON - Broken StreetLight 3", StreetLightSubscriber.State.first, StreetLightSubscriber.State.last,
        StreetLightSubscriber.State.error);
        local01.addTransition(StreetLightSubscriber.State.first, StreetLightSubscriber.State.processNeuralNetwork, LogStreetValues.AgentType.lightContainer, 
                LogStreetValues.AgentName.node3, 
                LogStreetValues.Action.receiveNeuralNetworkCommand, 
                LogStreetValues.Resource.ApdativeAgent, LogStreetValues.TypeLog.INFO);
        local01.addTransition(StreetLightSubscriber.State.processNeuralNetwork, StreetLightSubscriber.State.switchLightON, LogStreetValues.AgentType.lightContainer, 
                LogStreetValues.AgentName.node3, 
                LogStreetValues.Action.switchLightON, 
                LogStreetValues.Resource.streetlight, LogStreetValues.TypeLog.INFO);
        local01.addTransition(StreetLightSubscriber.State.switchLightON, StreetLightSubscriber.State.detectLight, LogStreetValues.AgentType.lightContainer, 
                LogStreetValues.AgentName.node3, 
                LogStreetValues.Action.detectLight, 
                LogStreetValues.Resource.streetlight, LogStreetValues.TypeLog.INFO);
        local01.addTransition(StreetLightSubscriber.State.detectLight, StreetLightSubscriber.State.processNeuralNetwork, LogStreetValues.AgentType.lightContainer, 
                LogStreetValues.AgentName.node3, 
                LogStreetValues.Action.receiveNeuralNetworkCommand, 
                LogStreetValues.Resource.ApdativeAgent, LogStreetValues.TypeLog.INFO);
        local01.addTransition(StreetLightSubscriber.State.detectLight, StreetLightSubscriber.State.last, LogStreetValues.AgentType.lightContainer, 
                LogStreetValues.AgentName.lights, 
                LogStreetValues.Action.finishSimulation, 
                LogStreetValues.Resource.simulation, LogStreetValues.TypeLog.INFO);
        
        local01.addTransition(StreetLightSubscriber.State.processNeuralNetwork, StreetLightSubscriber.State.switchLightOFF, LogStreetValues.AgentType.lightContainer, 
                LogStreetValues.AgentName.node3, 
                LogStreetValues.Action.switchLightOFF, 
                LogStreetValues.Resource.streetlight, LogStreetValues.TypeLog.INFO);
//        local01.addTransition(StreetLightSubscriber.State.switchLightOFF, StreetLightSubscriber.State.error, LogStreetValues.AgentType.lightContainer, 
//                LogStreetValues.AgentName.node3, 
//                LogStreetValues.Action.detectLight, 
//                LogStreetValues.Resource.streetlight, LogStreetValues.TypeLog.INFO);
        local01.addTransition(StreetLightSubscriber.State.switchLightOFF, StreetLightSubscriber.State.processNeuralNetwork, LogStreetValues.AgentType.lightContainer, 
                LogStreetValues.AgentName.node3, 
                LogStreetValues.Action.receiveNeuralNetworkCommand, 
                LogStreetValues.Resource.ApdativeAgent, LogStreetValues.TypeLog.INFO);
        local01.addTransition(StreetLightSubscriber.State.switchLightOFF, StreetLightSubscriber.State.last, LogStreetValues.AgentType.lightContainer, 
                LogStreetValues.AgentName.lights, 
                LogStreetValues.Action.finishSimulation, 
                LogStreetValues.Resource.simulation, LogStreetValues.TypeLog.INFO);
        
        StateMachine local02 = new StateMachine("SwitchLightON - StreetLight 10", StreetLightSubscriber.State.first, StreetLightSubscriber.State.last,
        StreetLightSubscriber.State.error);
        local02.addTransition(StreetLightSubscriber.State.first, StreetLightSubscriber.State.processNeuralNetwork, LogStreetValues.AgentType.lightContainer, 
                LogStreetValues.AgentName.node10, 
                LogStreetValues.Action.receiveNeuralNetworkCommand, 
                LogStreetValues.Resource.ApdativeAgent, LogStreetValues.TypeLog.INFO);
        local02.addTransition(StreetLightSubscriber.State.processNeuralNetwork, StreetLightSubscriber.State.switchLightON, LogStreetValues.AgentType.lightContainer, 
                LogStreetValues.AgentName.node10, 
                LogStreetValues.Action.switchLightON, 
                LogStreetValues.Resource.streetlight, LogStreetValues.TypeLog.INFO);
        local02.addTransition(StreetLightSubscriber.State.switchLightON, StreetLightSubscriber.State.detectLight, LogStreetValues.AgentType.lightContainer, 
                LogStreetValues.AgentName.node10, 
                LogStreetValues.Action.detectLight, 
                LogStreetValues.Resource.streetlight, LogStreetValues.TypeLog.INFO);
        local02.addTransition(StreetLightSubscriber.State.detectLight, StreetLightSubscriber.State.processNeuralNetwork, LogStreetValues.AgentType.lightContainer, 
                LogStreetValues.AgentName.node10, 
                LogStreetValues.Action.receiveNeuralNetworkCommand, 
                LogStreetValues.Resource.ApdativeAgent, LogStreetValues.TypeLog.INFO);
        local02.addTransition(StreetLightSubscriber.State.detectLight, StreetLightSubscriber.State.last, LogStreetValues.AgentType.lightContainer, 
                LogStreetValues.AgentName.lights, 
                LogStreetValues.Action.finishSimulation, 
                LogStreetValues.Resource.simulation, LogStreetValues.TypeLog.INFO);
        
        local02.addTransition(StreetLightSubscriber.State.processNeuralNetwork, StreetLightSubscriber.State.switchLightOFF, LogStreetValues.AgentType.lightContainer, 
                LogStreetValues.AgentName.node10, 
                LogStreetValues.Action.switchLightOFF, 
                LogStreetValues.Resource.streetlight, LogStreetValues.TypeLog.INFO);
//        local02.addTransition(StreetLightSubscriber.State.switchLightOFF, StreetLightSubscriber.State.error, LogStreetValues.AgentType.lightContainer, 
//                LogStreetValues.AgentName.node2, 
//                LogStreetValues.Action.detectLight, 
//                LogStreetValues.Resource.streetlight, LogStreetValues.TypeLog.INFO);
        local02.addTransition(StreetLightSubscriber.State.switchLightOFF, StreetLightSubscriber.State.processNeuralNetwork, LogStreetValues.AgentType.lightContainer, 
                LogStreetValues.AgentName.node10, 
                LogStreetValues.Action.receiveNeuralNetworkCommand, 
                LogStreetValues.Resource.ApdativeAgent, LogStreetValues.TypeLog.INFO);
        local02.addTransition(StreetLightSubscriber.State.switchLightOFF, StreetLightSubscriber.State.last, LogStreetValues.AgentType.lightContainer, 
                LogStreetValues.AgentName.lights, 
                LogStreetValues.Action.finishSimulation, 
                LogStreetValues.Resource.simulation, LogStreetValues.TypeLog.INFO);
        
        
        //global1.begin();
        this.stateMachineList.add(local01);
        this.stateMachineList.add(local02);
    }

    @Override
    public void testGui() {
        new IntegrationTestDemo().showGui("Switch the Light");
    }
    
}
