/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.puc.rio.les.testapp.streetlight.subscriber;

import br.pucrio.les.mas.subscriber.framework.functional.MessageObservable;
import br.pucrio.les.mas.subscriber.framework.functional.IntegrationTestDemo;
import br.pucrio.les.mas.subscriber.framework.BindingKey;
import br.pucrio.les.mas.subscriber.framework.LogContext;
import br.pucrio.les.mas.subscriber.framework.RabbitMQConsumer;
import br.pucrio.les.mas.subscriber.framework.statemachine.StateMachine;
import br.pucrio.les.mas.subscriber.framework.statemachine.StateValues;
import br.pucrio.les.mas.subscriber.framework.statemachine.results.FailedTest;
import br.pucrio.les.mas.subscriber.framework.statemachine.results.SuccessTest;
import br.pucrio.les.mas.subscriber.framework.statemachine.results.WrongTest;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nathalianascimento
 */
public class StreetLightObserverAgent extends RabbitMQConsumer{

    @Override
    protected void processData(LogContext log) {
        //log.printLogValues();
         MessageObservable.getInstance().changeData(log);
         
    }

    @Override
    public String getBindingKey() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String[] getListBindingKey() {
        BindingKey bd = new BindingKey();
        String[] listKey = new String[1];
        listKey[0] = bd.createBindingKey(LogStreetValues.AgentType.OBSERVER);
        return listKey;
    }

    @Override
    public void testGui() {
        new IntegrationTestDemo().showGui("Observer Agent Logs");
    }
   
    public enum State implements StateValues.State {
        first,setNeuralNetwork,readSimulationResults,calculateEnergy,calculatePeople,
        calculateTripDuration,last;
    }
    @Override
    public void createStateMachines() {
        
    }
    
}
