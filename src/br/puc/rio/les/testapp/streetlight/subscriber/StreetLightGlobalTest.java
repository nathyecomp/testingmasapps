/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.puc.rio.les.testapp.streetlight.subscriber;

import br.pucrio.les.mas.subscriber.framework.functional.MessageObservable;
import br.pucrio.les.mas.subscriber.framework.functional.IntegrationTestDemo;
import br.pucrio.les.mas.subscriber.framework.BindingKey;
import br.pucrio.les.mas.subscriber.framework.LogContext;
import br.pucrio.les.mas.subscriber.framework.RabbitMQConsumer;
import br.pucrio.les.mas.subscriber.framework.statemachine.StateMachine;
import br.pucrio.les.mas.subscriber.framework.statemachine.StateValues;
import br.pucrio.les.mas.subscriber.framework.statemachine.results.ExceededTimeTest;
import br.pucrio.les.mas.subscriber.framework.statemachine.results.FailedTest;
import br.pucrio.les.mas.subscriber.framework.statemachine.results.SuccessTest;
import br.pucrio.les.mas.subscriber.framework.statemachine.results.WrongTest;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nathalianascimento
 */
public class StreetLightGlobalTest extends RabbitMQConsumer{

    @Override
    protected void processData(LogContext log) {
        //log.printLogValues();
         MessageObservable.getInstance().changeData(log);
         
         this.processStateMachines(log);
    }

    @Override
    public String getBindingKey() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String[] getListBindingKey() {
        BindingKey bd = new BindingKey();
        String[] listKey = new String[8];
        
        listKey[0] = bd.createBindingKey(LogStreetValues.AgentType.OBSERVER, LogStreetValues.Action.startExecutionWithControllerConfiguration);
        listKey[1] = bd.createBindingKey(LogStreetValues.AgentType.OBSERVER,LogStreetValues.Action.readSimulationResults);
        listKey[2] = bd.createBindingKey(LogStreetValues.AgentType.OBSERVER,LogStreetValues.Action.calculateEnergy);
        listKey[3] = bd.createBindingKey(LogStreetValues.AgentType.OBSERVER,LogStreetValues.Action.calculateTimeTrip);
        listKey[4] = bd.createBindingKey(LogStreetValues.AgentType.OBSERVER,LogStreetValues.Action.calculatePeople);
        listKey[5] = bd.createBindingKey(LogStreetValues.AgentType.OBSERVER,LogStreetValues.Action.calculateFitness);
        listKey[6] = bd.createBindingKey(LogStreetValues.AgentType.OBSERVER,LogStreetValues.Action.achieveEnergyTarget);
        listKey[7] = bd.createBindingKey(LogStreetValues.AgentType.OBSERVER,LogStreetValues.Action.achievePeopleTarget);
        
//        listKey[7] = bd.createBindingKey(LogStreetValues.AgentType.OBSERVER,LogStreetValues.Action.chooseAdaptationMethod);
//        listKey[8] = bd.createBindingKey(LogStreetValues.AgentType.OBSERVER,LogStreetValues.Action.startGeneticAlgorithm);

        return listKey;
    }


    @Override
    public void testGui() {
        new IntegrationTestDemo().showGui("Evaluate Learning Selected Solution");
    }
   
    public enum State implements StateValues.State {
        first,setNeuralNetwork,readSimulationResults,calculateEnergy,calculatePeople,
        calculateTripDuration,last,error;
    }
    @Override
    public void createStateMachines() {
        StateMachine global1 = new StateMachine("EvaluateSolution", State.first, State.last,State.error);
        global1.addTransition(State.first, State.setNeuralNetwork, LogStreetValues.AgentType.OBSERVER, 
                LogStreetValues.AgentName.OBSERVER, 
                LogStreetValues.Action.startExecutionWithControllerConfiguration, 
                LogStreetValues.Resource.individual, LogStreetValues.TypeLog.INFO);
        global1.addTransition(State.setNeuralNetwork, State.readSimulationResults,
                LogStreetValues.AgentType.OBSERVER, 
                LogStreetValues.AgentName.OBSERVER, 
                LogStreetValues.Action.readSimulationResults, 
                LogStreetValues.Resource.individual, LogStreetValues.TypeLog.INFO);
        global1.addTransition(State.readSimulationResults, State.calculateEnergy,
                LogStreetValues.AgentType.OBSERVER, 
                LogStreetValues.AgentName.OBSERVER, 
                LogStreetValues.Action.calculateEnergy, 
                LogStreetValues.Resource.individual, LogStreetValues.TypeLog.INFO);
        global1.addTransition(State.calculateEnergy, State.calculatePeople,
                LogStreetValues.AgentType.OBSERVER, 
                LogStreetValues.AgentName.OBSERVER, 
                LogStreetValues.Action.achieveEnergyTarget, 
                LogStreetValues.Resource.individual, LogStreetValues.TypeLog.INFO);
        global1.addTransition(State.calculatePeople, State.calculateTripDuration,
                LogStreetValues.AgentType.OBSERVER, 
                LogStreetValues.AgentName.OBSERVER, 
                LogStreetValues.Action.achievePeopleTarget, 
                LogStreetValues.Resource.individual, LogStreetValues.TypeLog.INFO);
        global1.addTransition(State.calculateTripDuration, State.last,
                LogStreetValues.AgentType.OBSERVER, 
                LogStreetValues.AgentName.OBSERVER, 
                LogStreetValues.Action.calculateFitness, 
                LogStreetValues.Resource.individual, LogStreetValues.TypeLog.INFO);
        
        //global1.begin();
        this.stateMachineList.add(global1);
    }
    
}
