/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.puc.rio.les.testapp.streetlight.subscriber;

import br.pucrio.les.mas.subscriber.framework.functional.MessageObservable;
import br.pucrio.les.mas.subscriber.framework.functional.IntegrationTestDemo;
import br.pucrio.les.mas.subscriber.framework.BindingKey;
import br.pucrio.les.mas.subscriber.framework.LogContext;
import br.pucrio.les.mas.subscriber.framework.RabbitMQConsumer;
import br.pucrio.les.mas.subscriber.framework.statemachine.StateValues;

/**
 *
 * @author nathalianascimento
 */
public class StreetLightAllAgents extends RabbitMQConsumer{

    @Override
    protected void processData(LogContext log) {
        //log.printLogValues();
         MessageObservable.getInstance().changeData(log);
         
    }

    @Override
    public String getBindingKey() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String[] getListBindingKey() {
        BindingKey bd = new BindingKey();
        String[] listKey = new String[5];
        listKey[0] = bd.createBindingKey(LogStreetValues.AgentType.OBSERVER);
        listKey[1] = bd.createBindingKey(LogStreetValues.AgentType.lightContainer);
        listKey[2] = bd.createBindingKey(LogStreetValues.AgentType.MANAGER);
        listKey[3] = bd.createBindingKey(LogStreetValues.AgentType.AdaptiveAgent);
        listKey[4] = bd.createBindingKey(LogStreetValues.AgentType.lights);
        return listKey;
    }

    @Override
    public void testGui() {
        new IntegrationTestDemo().showGui("Agent Logs");
    }
   
    public enum State implements StateValues.State {
        first,setNeuralNetwork,readSimulationResults,calculateEnergy,calculatePeople,
        calculateTripDuration,last;
    }
    @Override
    public void createStateMachines() {
        
    }
    
}
