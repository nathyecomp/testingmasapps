/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.puc.rio.les.testapp.fiot.subscriber;

import br.puc.rio.les.testapp.streetlight.subscriber.*;
import br.pucrio.les.mas.subscriber.framework.functional.MessageObservable;
import br.pucrio.les.mas.subscriber.framework.functional.IntegrationTestDemo;
import br.pucrio.les.mas.subscriber.framework.BindingKey;
import br.pucrio.les.mas.subscriber.framework.LogContext;
import br.pucrio.les.mas.subscriber.framework.RabbitMQConsumer;

/**
 *
 * @author nathalianascimento
 */
public class FIoTSubscriber extends RabbitMQConsumer{

    @Override
    protected void processData(LogContext log) {
        //log.printLogValues();
         MessageObservable.getInstance().changeData(log);
    }

    @Override
    public String getBindingKey() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String[] getListBindingKey() {
        BindingKey bd = new BindingKey();
        String[] listKey = new String[3];
        listKey[0] = bd.createBindingKey(LogFIoTValues.AgentType.AdaptiveAgent);
     //  // listKey[1] = bd.createBindingKey(LogStreetValues.Action.calculateEnergy);
        listKey[1] = bd.createBindingKey(LogFIoTValues.AgentType.MANAGER);
        
        listKey[2] = bd.createBindingKey(LogFIoTValues.AgentType.OBSERVER);
        return listKey;
    }

    @Override
    public void testGui() {
        new IntegrationTestDemo().showGui();
    }

    @Override
    public void createStateMachines() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
