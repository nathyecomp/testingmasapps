/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.puc.rio.les.testapp.fiot.subscriber;

import br.puc.rio.les.testapp.streetlight.subscriber.*;
import br.pucrio.les.mas.subscriber.framework.LogValues;



/**
 *
 * @author nathalianascimento
 */
public class LogFIoTValues implements LogValues{
    
   public enum Action implements LogValues.Action {
    }
    public enum TypeLog implements LogValues.TypeLog{
        INFO, WARNING, ERROR;
    }
    public enum Resource implements LogValues.Resource{
         
    }
    public enum AgentType implements LogValues.AgentType{
        AdaptiveAgent, OBSERVER, MANAGER;
    }
   
}
