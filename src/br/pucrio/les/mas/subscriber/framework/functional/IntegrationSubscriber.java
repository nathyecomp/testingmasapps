/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pucrio.les.mas.subscriber.framework.functional;

import br.pucrio.les.testapp.booksale.subscriber.functional.LogFunctionalValue;
import br.pucrio.les.mas.subscriber.framework.BindingKey;
import br.pucrio.les.mas.subscriber.framework.LogContext;
import br.pucrio.les.mas.subscriber.framework.RabbitMQConsumer;

/**
 *
 * @author nathalianascimento
 */
public class IntegrationSubscriber extends RabbitMQConsumer{

    @Override
    protected void processData(LogContext log) {
        //log.printLogValues();
         MessageObservable.getInstance().changeData(log);
    }

    @Override
    public String getBindingKey() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String[] getListBindingKey() {
        BindingKey bd = new BindingKey();
        String[] listKey = new String[11];
        listKey[0] = bd.createBindingKey(LogFunctionalValue.Action.isDestroyed);
        listKey[1] = bd.createBindingKey(LogFunctionalValue.Action.connectToSystem);
        listKey[2] = bd.createBindingKey(LogFunctionalValue.AgentType.CLIENT, LogFunctionalValue.Action.searchBook);
        listKey[3] = bd.createBindingKey(LogFunctionalValue.AgentType.CLIENT, LogFunctionalValue.Action.selectBestPrice);
        listKey[4] = bd.createBindingKey(LogFunctionalValue.AgentType.CLIENT, LogFunctionalValue.Action.askBuy);
        listKey[5] = bd.createBindingKey(LogFunctionalValue.AgentType.CLIENT, LogFunctionalValue.Action.receivedSaleConfirmation);
        listKey[6] = bd.createBindingKey(LogFunctionalValue.AgentType.CLIENT, LogFunctionalValue.Action.receivedBook);
        listKey[7] = bd.createBindingKey(LogFunctionalValue.AgentType.SELLER, LogFunctionalValue.Action.registerService);
        listKey[8] = bd.createBindingKey(LogFunctionalValue.AgentType.SELLER, LogFunctionalValue.Action.addBook);
        listKey[9] = bd.createBindingKey(LogFunctionalValue.AgentType.SELLER, LogFunctionalValue.Action.receivedBuy);
        listKey[10] = bd.createBindingKey(LogFunctionalValue.AgentType.SELLER, LogFunctionalValue.Action.removeBook);
        return listKey;
    }

    @Override
    public void testGui() {
        new IntegrationTestDemo().showGui();
    }

    @Override
    public void createStateMachines() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
