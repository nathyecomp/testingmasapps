/**
 *
 */
package br.pucrio.les.mas.subscriber.framework.functional;

import br.pucrio.les.mas.subscriber.framework.LogContext;
import br.pucrio.les.mas.subscriber.framework.functional.gui.TMASLogContextTableModel;
import br.pucrio.les.mas.subscriber.framework.functional.gui.TMASLogTablePane;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;


/**
 * Integration test class
 *
 * @author cviana
 */
public class IntegrationTestDemo implements Observer {
    
    public IntegrationTestDemo(){
        MessageObservable.getInstance().addObserver(this);
    }
  private static TMASLogContextTableModel model;

  /**
   * Create the GUI and show it. For thread safety, this method should be
   * invoked from the event-dispatching thread.
   */
  private static void createAndShowGUI(String testName) {
    // Make sure we have nice window decorations.
    JFrame.setDefaultLookAndFeelDecorated(true);

    // Create table panel.
    JPanel panel = new JPanel(new BorderLayout());
    panel.add(buildTablePane(), BorderLayout.CENTER);

    // Create and set up the window.
    JFrame frame = new JFrame(testName);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setPreferredSize(new Dimension(800, 600));
    frame.setContentPane(panel);

    // Display the window.
    frame.pack();
    frame.setVisible(true);
  }

  /**
   * Build table panel.
   *
   * @return the table panel.
   */
  private static JComponent buildTablePane() {
    model = new TMASLogContextTableModel(createTableData());
    //    TMASLogContextTableModel model = new TMASLogContextTableModel();
    return new TMASLogTablePane(model);
  }

  /**
   * Create fake table data.
   *
   * @return table data.
   */
  private static List<LogContext> createTableData() {
    List<LogContext> data = new ArrayList<>();

    LogContext lc1 = new LogContext();
    lc1.setAgentType("Seller");
    lc1.setAgentName("Seller1");
    lc1.setTypeLog("Error");
    lc1.setAction("BuyBook");
    lc1.setMillisecond("10101010");
    lc1.setMessage("Vou comprar o livro 1.");

    LogContext lc2 = new LogContext();
    lc2.setAgentType("Client");
    lc2.setAgentName("Client1");
    lc2.setTypeLog("Info");
    lc2.setAction("BuyBook");
    lc2.setMillisecond("20202020");
    lc2.setMessage("Vou comprar o do livro 2.");

   // data.add(lc1);
    //data.add(lc2);

    return data;
  }

  /**
   * @param args
   */
  public static void showGui() {
    // Schedule a job for the event-dispatching thread:
    // creating and showing this application's GUI.
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        createAndShowGUI("INTEGRATION LOGS");
      }
    });
  }
  /**
     * @param testName
   */
  public static void showGui(final String testName) {
    // Schedule a job for the event-dispatching thread:
    // creating and showing this application's GUI.
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        createAndShowGUI(testName);
      }
    });
  }

  @Override
  public void update(Observable o, Object arg) {
    model.addRow((LogContext) arg);
  }

}
