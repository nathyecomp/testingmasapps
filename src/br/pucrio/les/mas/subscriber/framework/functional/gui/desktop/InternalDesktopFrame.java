package br.pucrio.les.mas.subscriber.framework.functional.gui.desktop;

///**
// * 
// */
//package br.pucrio.mas.subscriber.integration.gui.desktop;
//
//import java.awt.Dimension;
//import java.awt.GraphicsConfiguration;
//import java.awt.HeadlessException;
//import java.awt.Toolkit;
//import java.awt.event.ActionEvent;
//import java.awt.event.ActionListener;
//
//import javax.swing.JDesktopPane;
//import javax.swing.JFrame;
//
///**
// * Janela interna de um desktop
// * 
// * @author cviana
// */
//public class InternalDesktopFrame extends JFrame implements ActionListener {
//
//	private JDesktopPane desktop;
//	/**
//	 * @throws HeadlessException
//	 */
//	public InternalDesktopFrame() throws HeadlessException {
//        super("InternalFrameDemo");
//
//        //Make the big window be indented 50 pixels from each edge
//        //of the screen.
//        int inset = 50;
//        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
//        setBounds(inset, inset,
//                  screenSize.width  - inset*2,
//                  screenSize.height - inset*2);
//
//        //Set up the GUI.
//        desktop = new JDesktopPane(); //a specialized layered pane
//        createFrame(); //create first "window"
//        setContentPane(desktop);
//        setJMenuBar(createMenuBar());
//
//        //Make dragging a little faster but perhaps uglier.
//        desktop.setDragMode(JDesktopPane.OUTLINE_DRAG_MODE);
//	}
//
//
//	/**
//	 * @param title
//	 * @throws HeadlessException
//	 */
//	public InternalDesktopFrame(String title) throws HeadlessException {
//		super(title);
//		// TODO Auto-generated constructor stub
//	}
//
//	/**
//	 * @param title
//	 * @param gc
//	 */
//	public InternalDesktopFrame(String title, GraphicsConfiguration gc) {
//		super(title, gc);
//		// TODO Auto-generated constructor stub
//	}
//
//	/* (non-Javadoc)
//	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
//	 */
//	@Override
//	public void actionPerformed(ActionEvent e) {
//		// TODO Auto-generated method stub
//
//	}
//
//}
