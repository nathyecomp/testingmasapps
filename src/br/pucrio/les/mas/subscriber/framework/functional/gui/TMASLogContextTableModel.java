package br.pucrio.les.mas.subscriber.framework.functional.gui;

import br.pucrio.les.mas.subscriber.framework.LogContext;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.swing.table.AbstractTableModel;


/**
 * Log Table Model
 *
 * @author cviana
 */
public class TMASLogContextTableModel extends AbstractTableModel {

  /** Sortable Table rows */
  private List<LogContext> rows;

  /** Table coluns */
  private String[] coluns = { "Agent Type", "Agent Name", "Class", "Log Type", "Action",
      "Resource", "Timestamp(ms)","Message"};
     
      //"Month", "Day","Hour","Minute","Second", "Millisecond","Message" };

  /** Max rows */
  private int maxSize = 50;

  /**
   * Construtor.
   */
  public TMASLogContextTableModel() {
    super();
    this.rows = new ArrayList<LogContext>(maxSize);
  }

  /**
   * Construtor.
   *
   * @param rows table rows.
   */
  public TMASLogContextTableModel(List<LogContext> rows) {
    this();
    if (rows.size() < maxSize) {
      this.rows.addAll(rows);
    }
    else {
      this.rows.addAll(rows.subList(0, maxSize - 1));
    }
  }

  /**
   * Add one log context row.
   *
   * @param lc log context row.
   */
  public void addRow(LogContext lc) {
    if (rows.size() < maxSize) {
      rows.add(lc);
    }
    else {
      rows.add(maxSize - 1, lc);
    }
    fireTableDataChanged();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getColumnName(int column) {
    return coluns[column];
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int getRowCount() {
    return rows.size();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int getColumnCount() {
    return coluns.length;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Object getValueAt(int rowIndex, int columnIndex) {
    switch (columnIndex) {
      case 0:
        return rows.get(rowIndex).getAgentType();
      case 1:
        return rows.get(rowIndex).getAgentName();
      case 2:
        return rows.get(rowIndex).getClassName();
      case 3:
        return rows.get(rowIndex).getTypeLog();
      case 4:
        return rows.get(rowIndex).getAction();
      case 5:
        return rows.get(rowIndex).getResource();
      case 6:
        return rows.get(rowIndex).getTotalMilliseconds();
      case 7:
        return rows.get(rowIndex).getMessage();
     /* case 4:
        return rows.get(rowIndex).getMonth();
      case 5:
        return rows.get(rowIndex).getDay();
      case 6:
        return rows.get(rowIndex).getHour();
      case 7:
        return rows.get(rowIndex).getMinute();
      case 8:
        return rows.get(rowIndex).getSecond();
      case 9:
        return rows.get(rowIndex).getMillisecond();
      case 10:
        return rows.get(rowIndex).getMessage();*/
    }
    return "";
  }

  /**
   * Get a Log context comparator.
   *
   * @return comparator.
   */
  private Comparator<LogContext> getLogContextComparator() {
    return new Comparator<LogContext>() {

      @Override
      public int compare(LogContext o1, LogContext o2) {
        return new Integer(o2.getMillisecond()).compareTo(new Integer(o1
          .getMillisecond()));
      }
    };
  }

}
