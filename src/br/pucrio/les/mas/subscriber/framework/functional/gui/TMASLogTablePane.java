/**
 *
 */
package br.pucrio.les.mas.subscriber.framework.functional.gui;

import br.pucrio.les.mas.subscriber.framework.LogContext;
import br.pucrio.les.mas.subscriber.framework.functional.util.table.TableUtils;
import java.awt.Component;
import java.awt.Dimension;
import java.util.List;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;


/**
 * Log table panel.
 *
 * @author cviana
 */
public class TMASLogTablePane extends JScrollPane {

  private static final long serialVersionUID = 1L;

  /** Main table. */
  private JTable table;

  /**
   * Construtor.
   *
   * @param tableModel table model.
   */
  public TMASLogTablePane(TMASLogContextTableModel tableModel) {
    table = createTable(tableModel);
    adjustColumnsWidth(table);
    setViewportView(table);
  }

  /**
   * Table model.
   *
   * @param rows table model lines
   * @return the model.
   */
  private AbstractTableModel createTableModel(List<LogContext> rows) {
    return new TMASLogContextTableModel(rows);
  }

  /**
   * Create table.
   *
   * @param model model for new table.
   * @return a new table.
   */
  private JTable createTable(AbstractTableModel model) {
    final JTable table = new JTable(model) {
      @Override
      public Dimension getPreferredScrollableViewportSize() {
        Dimension size = getPreferredSize();
        size.width += 5;
        return size;
      }

      @Override
      public boolean getScrollableTracksViewportWidth() {
        if (autoResizeMode != AUTO_RESIZE_OFF) {
          if (getParent() instanceof JViewport) {
            return (((JViewport) getParent())
              .getWidth() > getPreferredSize().width);
          }
        }
        return false;
      }

      @Override
      public Component prepareRenderer(TableCellRenderer renderer, int row,
        int column) {
        int viewIdx = row;
        int modelIdx = convertRowIndexToModel(viewIdx);
        Component c = super.prepareRenderer(renderer, row, column);
        c.setBackground(TableUtils.getBackgroundRowColor(modelIdx));
        return c;
      }
    };

    table.setAutoCreateRowSorter(true);
    table.setRowSelectionAllowed(false);
    table.setCellSelectionEnabled(false);
    return table;
  }

  /**
   * Adjusting table column width.
   *
   * @param table the table.
   */
  private void adjustColumnsWidth(JTable table) {
    TableModel tableModel = table.getModel();
    TableColumnModel columnModel = table.getColumnModel();
    TableCellRenderer headerRenderer = table.getTableHeader()
      .getDefaultRenderer();
    // For all columns
    for (int cIdx = 0; cIdx < table.getColumnCount(); cIdx++) {
      String columnName = table.getColumnName(cIdx);
      // Define column header as default width.
      Component header = headerRenderer.getTableCellRendererComponent(table,
        columnName, false, false, 0, cIdx);
      int headerPreferenceWidth = header.getPreferredSize().width;
      // Get table column renderer.
      TableColumn column = columnModel.getColumn(cIdx);
      TableCellRenderer columnRenderer = column.getCellRenderer();
      if (null == columnRenderer) {
        columnRenderer = table.getDefaultRenderer(tableModel.getColumnClass(
          cIdx));
        if (null == columnRenderer) {
          columnRenderer = new DefaultTableCellRenderer();
        }
      }
      // For all table rows
      for (int rIdx = 0; rIdx < tableModel.getRowCount(); rIdx++) {
        Object value = tableModel.getValueAt(rIdx, cIdx);
        if (value != null) {
          Component component = columnRenderer.getTableCellRendererComponent(
            table, value, false, false, rIdx, cIdx);
          int width = component.getPreferredSize().width;
          headerPreferenceWidth = Math.max(headerPreferenceWidth, width);
        }
      }
      TableColumn aColumnModel = columnModel.getColumn(cIdx);
      aColumnModel.setPreferredWidth(headerPreferenceWidth + 2);
    }
  }
}
