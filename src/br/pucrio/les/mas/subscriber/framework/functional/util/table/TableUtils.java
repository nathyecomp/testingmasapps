package br.pucrio.les.mas.subscriber.framework.functional.util.table;



import java.awt.Color;

/**
 * Auxiliary table class.
 *
 * @author cviana
 */
public class TableUtils {

  /**
   * Get background table row color.
   *
   * @param row table row index
   *
   * @return background row color.
   */
  public static Color getBackgroundRowColor(int row) {
    if ((row % 2) == 0) {
      return Color.WHITE;
    }
    // Cinza claro.
    return new Color(240, 240, 240);
  }

}
