/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pucrio.les.mas.subscriber.framework.statemachine.results;

import br.pucrio.les.mas.subscriber.framework.statemachine.StateValues;

public class WrongTest extends Exception {    
    public WrongTest(String machineName, StateValues.State state) {
        super(machineName+": state is missing - " + state.name());
    }
}