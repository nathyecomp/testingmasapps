/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pucrio.les.mas.subscriber.framework.statemachine;

import br.pucrio.les.mas.subscriber.framework.LogContext;
import br.pucrio.les.mas.subscriber.framework.LogValues;
import br.pucrio.les.mas.subscriber.framework.statemachine.results.ExceededTimeTest;
import br.pucrio.les.mas.subscriber.framework.statemachine.results.FailedTest;
import br.pucrio.les.mas.subscriber.framework.statemachine.results.SuccessTest;
import br.pucrio.les.mas.subscriber.framework.statemachine.results.WrongTest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author nathalianascimento
 */
public class StateMachine {
    
    HashMap<StateValues.State,List<Transition>> machine;
    StateValues.State first,current,last,error;
    long start = 0;
    final long timeExceed = 1000000;
    String machineName;
    boolean begin = false;
    boolean failed = true;
    
    public StateMachine(String machineName, StateValues.State first, StateValues.State last, StateValues.State error){
        this.machineName = machineName;
        this.machine = new HashMap<>();
        this.first = first;
        this.last = last;
        this.error = error;
        this.current = first;
        this.begin();
    }
    
    private void begin(){
        this.current = first;
        this.startState();
    }

    public boolean isFailed() {
        return failed;
    }
    
    
    public StateValues.State getNext(LogContext log) throws FailedTest, SuccessTest, WrongTest, ExceededTimeTest{
        if(!this.hasNext()){
            throw new SuccessTest(this.machineName);
        }
        if(!machine.containsKey(this.current)){
            throw new WrongTest(this.machineName,this.current);
        }
        List<Transition> transitions = machine.get(this.current);
        for(Transition tr: transitions){
            if(tr.getValue().compareLogContext(log)){
                this.current = tr.getEnd();
                this.startState();
                 if(!this.hasNext()){
                     //como o timing do log pode variar, pode acontecer de acabar todos os logs, e nao entrar mais no metodo
                     //para ver q ja falhou (q o tempo excedeu)
                     this.failed = false;
                    throw new SuccessTest(this.machineName);
                 }
                 else if(this.current.name().equals(this.error.name())){
                     throw new FailedTest(this.machineName,this.current,log);
                 }
                return tr.getEnd();
            }
            
        }
        if(!this.current.name().equals(this.first.name()) && this.isTimeExceeded()){
                throw new ExceededTimeTest(this.machineName,this.current,log);
        }
        return this.current;
    }
    
    public void addTransition(StateValues.State begin, StateValues.State end,
            LogValues.AgentType agentType, LogValues.AgentName agentName, 
            LogValues.Action action, LogValues.Resource resource, 
            LogValues.TypeLog typeLog){
        
        LogContext log = new LogContext(agentType, agentName, action, resource, typeLog);
        Transition transition = new Transition(begin, end, log);
        if(!machine.containsKey(begin)){
            List<Transition> transitions = new ArrayList<>();
            machine.put(begin, transitions);
        }
        machine.get(begin).add(transition);
    }
    private boolean hasNext(){
        return (!this.current.name().equals(this.last.name()));
    }
    
    private void startState(){
        start = System.currentTimeMillis();
    }
    private boolean isTimeExceeded(){
        return false; //o teste acabou sendo feito comparando os ultimos estados no Junit
       // return System.currentTimeMillis()-start >timeExceed;
    }

    public StateValues.State getFirst() {
        return first;
    }

    public StateValues.State getLast() {
        return last;
    }

    public StateValues.State getCurrent() {
        return current;
    }
    
    
    
    
}
