/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pucrio.les.mas.subscriber.framework.statemachine;

import br.pucrio.les.mas.subscriber.framework.LogContext;

/**
 *
 * @author nathalianascimento
 */
public class Transition {
    StateValues.State begin, end;
    LogContext value;
    
    public Transition(StateValues.State begin, StateValues.State end, LogContext value){
        this.begin = begin;
        this.end = end;
        this.value = value;
    }

    public StateValues.State getBegin() {
        return begin;
    }

    public StateValues.State getEnd() {
        return end;
    }

    public LogContext getValue() {
        return value;
    }
    
}
