/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pucrio.les.mas.subscriber.framework.statemachine.results;

import br.pucrio.les.mas.subscriber.framework.LogContext;
import br.pucrio.les.mas.subscriber.framework.statemachine.StateValues;

public class FailedTest extends Exception {    
    public FailedTest(String machineName, StateValues.State state, LogContext log) {
        super(machineName+": Error was generated at state - " + state.name() +" received log "+
                log.getLogText());
    }
}