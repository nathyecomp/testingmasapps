/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pucrio.les.mas.subscriber.framework.statemachine.results;

import br.pucrio.les.mas.subscriber.framework.LogContext;
import br.pucrio.les.mas.subscriber.framework.statemachine.StateValues;

public class ExceededTimeTest extends Exception {    
    public ExceededTimeTest(String machineName, StateValues.State state, LogContext log) {
        super(machineName+": Exceeded Time at state - " + state.name() +" waiting log "+
                log.getLogText());
    }
}