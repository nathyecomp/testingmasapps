/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pucrio.les.mas.subscriber.framework.statemachine.results;

public class SuccessTest extends Exception {    
    public SuccessTest(String machineName) {
        super(machineName+": Successfull test!");
    }
}