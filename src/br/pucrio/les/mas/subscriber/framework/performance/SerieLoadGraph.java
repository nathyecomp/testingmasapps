/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pucrio.les.mas.subscriber.framework.performance;

import java.util.HashMap;
import java.util.List;

/**
 *
 * @author nathalianascimento
 */
public class SerieLoadGraph {
    int numResponseAgents; //number of agentes that can responde the requests
    int numActions;
    int numActionsExecuted;
    HashMap actionsTime;
    List<String> listActions;
    String firstAction;
    String unsteadyAction;
    double first_Action_Time;
    public int numTimesUnsteadyAction;
    
    
    

    public SerieLoadGraph(List<String> listActions, String firstAction, String unsteadyAction) {
        this.first_Action_Time = -1;
        this.unsteadyAction = unsteadyAction;
        this.firstAction = firstAction;
        this.actionsTime = new HashMap<>();
        this.listActions = listActions;
        this.initializeMap();
        
    }
    
    public final void initializeMap(){
        this.numTimesUnsteadyAction = 0;
        this.numResponseAgents = 0;
        this.numActionsExecuted = 0;
        this.numActions = listActions.size();
        this.first_Action_Time = -1;
        for (String action : listActions) {
            this.actionsTime.put(action, -1);
	}
    }
    
    public void setActionTime(String action, double time){
        if(action.equals(this.unsteadyAction)){
            this.numTimesUnsteadyAction++;
        }
        if(action.equals(this.firstAction) && this.first_Action_Time==-1){
            this.first_Action_Time = time;
        }
        if(this.actionsTime.containsKey(action) && this.actionsTime.get(action).equals(-1)){
            this.numActionsExecuted++;
            this.actionsTime.put(action, time);
        }
    }
    
    
    public double[][] getSeries() {
        int num = this.numActions;
        double[][] series = new double[2][num];
        for (int i = 0; i < num; i++) {
            String action = this.listActions.get(i);
            series[0][i] = (double) i;
            double differTime;
            if(this.actionsTime.get(action).equals(-1)){
                differTime = 0;
            }
            else{
                differTime = ((double) this.actionsTime.get(action)) - this.first_Action_Time;
            }
            series[1][i] = differTime;
            
        }
        return series;
    }

    
    
    public int getNumActionsExecuted() {
        return numActionsExecuted;
    }
    public int getNumResponseAgents() {
        return numResponseAgents;
    }

    public void setNumResponseAgents(int numResponseAgents) {
        this.numResponseAgents = numResponseAgents;
    }
    
      public int getNumTimesUnsteadyAction() {
        return numTimesUnsteadyAction;
    }
    
    
    
    
//     //for each connected seller, incremets numSellers. And if
//    //an agent is destroyed, it decrements numSellers
//    //connectToSystem and isDestroyed
//    int numSellers;
//    int numClients;
//    int totalAgents;
//    
//    String nameCurrentAgent;
//    
//    //for each connected seller, calculate the difference between 
//    //connectToSystem and registerService
//    //then, add it into a total value of timeToRegister and divide
//    //per the total number of connected agents
//    double timeToRegisterAverage;
//   //for each connected client, calculate the difference between 
//    //searchBook and selectBestPrice
//    //then, add it into a total value of timeToSelectBestPrice and divide
//    //per the total number of connected agents
//    double timeToSelectBestPrice;

  

    

    
    
    
}
