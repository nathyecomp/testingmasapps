package br.pucrio.les.mas.subscriber.framework.performance;


import br.pucrio.les.mas.subscriber.framework.performance.MessageObservable;
import br.pucrio.les.mas.subscriber.framework.performance.SerieLoadGraph;
import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.axis.SymbolAxis;
import org.jfree.chart.axis.TickUnitSource;
import org.jfree.chart.labels.StandardXYItemLabelGenerator;
import org.jfree.chart.labels.XYItemLabelGenerator;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.DefaultXYDataset;
import org.jfree.data.xy.XYDataset;

/**
 * @author John B. Matthews
 */
public class GuiPerformance extends JFrame implements Observer {
    
    private static int totalAgents = 5;
    private static int totalSellers = 0;
    private static int totalClients = 0;
    private static final Random random = new Random();

    private static final String title = "Performance Test";
    
    final DefaultXYDataset dataset;
    
   // private static String[] labelsDomain;
    
    
    /**
     * Construct a new frame 
     *
     * @param labelsDomain
     * @param title the frame title
     */
    public GuiPerformance(String[] labelsDomain) {
        super(title);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        dataset = new DefaultXYDataset();
        //dataset.addSeries("Time to register in Yellow Page", createSeries(0));
        //dataset.addSeries("Time to contact all sellers", createSeries(1));
        JFreeChart chart = createChart(dataset,2,3,labelsDomain);
        ChartPanel chartPanel = new ChartPanel(chart, false);
        chartPanel.setPreferredSize(new Dimension(640, 480));
        this.add(chartPanel, BorderLayout.CENTER);
 
        JPanel buttonPanel = new JPanel();
        JButton addButton = new JButton("Add Series");
        buttonPanel.add(addButton);
        addButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int n = dataset.getSeriesCount();
                dataset.addSeries("Series" + n, createSeries(n));
            }
        });
        JButton remButton = new JButton("Remove Series");
        buttonPanel.add(remButton);
        remButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int n = dataset.getSeriesCount() - 1;
                dataset.removeSeries("Series" + n);
            }
        });
       // this.add(buttonPanel, BorderLayout.SOUTH);
    }

    /**
     * Create a series
     * 
     * @ return the series
     */
    private double[][] createSeries(int mean) {
        double[][] series = new double[2][totalAgents];
        for (int i = 0; i < totalAgents; i++) {
            series[0][i] = (double) i;
            series[1][i] = mean + random.nextGaussian() / 2;
            
        }
        return series;
    }

    /**
     * Create a chart.
     *
     * @param dataset the dataset
     * @return the chart
     */
    private JFreeChart createChart(XYDataset dataset, int numSeller, int numClient, String[] labelsDomain) {

        // create the chart...
        String domainAxisLabel = "Actions executed by request agents";
        JFreeChart chart = ChartFactory.createXYLineChart(
            "Performance Test", // chart title
            domainAxisLabel, // domain axis label
            "Response Time (milliseconds)", // range axis label
            dataset,  // initial series
            PlotOrientation.VERTICAL, // orientation
            true, // include legend
            true, // tooltips?
            false // URLs?
            );


        // set chart background
        chart.setBackgroundPaint(Color.white);

        // set a few custom plot features
        XYPlot plot = (XYPlot) chart.getPlot();
        plot.setBackgroundPaint(new Color(0xffffe0));
        plot.setDomainGridlinesVisible(true);
        plot.setDomainGridlinePaint(Color.lightGray);
        plot.setRangeGridlinePaint(Color.lightGray);

        TickUnitSource ticks = NumberAxis.createIntegerTickUnits();
        NumberAxis range = (NumberAxis) plot.getRangeAxis();
        range.setStandardTickUnits(ticks);
        
    SymbolAxis rangeAxis = new SymbolAxis("Actions exeucted by a request agent", labelsDomain);
    rangeAxis.setStandardTickUnits(ticks);
    //rangeAxis.setTickUnit(new NumberTickUnit(1));
    rangeAxis.setRange(0,labelsDomain.length);
    plot.setDomainAxis(rangeAxis); 
        
        
        
        
        
        

        // render shapes and lines
        XYLineAndShapeRenderer renderer =
            new XYLineAndShapeRenderer(true, true);
        plot.setRenderer(renderer);
        renderer.setBaseShapesVisible(true);
        renderer.setBaseShapesFilled(true);

        // set the renderer's stroke
        Stroke stroke = new BasicStroke(
            3f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL);
        renderer.setBaseOutlineStroke(stroke);

        // label the points
        NumberFormat format = NumberFormat.getNumberInstance();
        format.setMaximumFractionDigits(2);
        XYItemLabelGenerator generator =
            new StandardXYItemLabelGenerator(
                StandardXYItemLabelGenerator.DEFAULT_ITEM_LABEL_FORMAT,
                format, format);
        renderer.setBaseItemLabelGenerator(generator);
        renderer.setBaseItemLabelsVisible(true);

        return chart;
    }

    public static int getTotalAgents() {
        return totalAgents;
    }

    public static void setTotalAgents(int totalAgents) {
        GuiPerformance.totalAgents = totalAgents;
    }

    public static int getTotalSellers() {
        return totalSellers;
    }

    public static void setTotalSellers(int totalSellers) {
        GuiPerformance.totalSellers = totalSellers;
    }

    public static int getTotalClients() {
        return totalClients;
    }

    public static void setTotalClients(int totalClients) {
        GuiPerformance.totalClients = totalClients;
    }
    
    

    /** Main method
     * @param labelsDomain */
    public static void execute(final String[] labelsDomain) {
        
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                GuiPerformance demo = new GuiPerformance(labelsDomain);
               // demo.labelsDomain = labelsDomain;
                //final MessageObservable observable = new MessageObservable();
                MessageObservable.getInstance().addObserver(demo);
                demo.pack();
                demo.setLocationRelativeTo(null);
                demo.setVisible(true);
            }
         });
    }

     @Override // Observer interface's implemented method
    public void update(Observable o, Object data) {	
         System.out.println("Update observer");
         SerieLoadGraph series = (SerieLoadGraph) data;
         double[][] serie = series.getSeries();
         int n = dataset.getSeriesCount();
                dataset.addSeries("Num Response Agents: " + series.getNumResponseAgents(), serie);
        //label.setText((String) data); // displays new text in JLabel
    }
}