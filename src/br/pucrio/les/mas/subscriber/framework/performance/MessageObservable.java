package br.pucrio.les.mas.subscriber.framework.performance;


import java.util.Observable;

public class MessageObservable extends Observable {
    
    public static MessageObservable instance;
    private MessageObservable() {	
        super();
    }
    public static MessageObservable getInstance() {

        if (instance == null) {
            inicializaInstancia();
        }
        return instance;
    }

    private static synchronized void inicializaInstancia() {
        if (instance == null) {
            instance = new MessageObservable();
        }
    }
    
    
    public void changeData(Object data) {
     //   System.out.println("CHANGED DATA "+data.toString());
        setChanged(); // the two methods of Observable class
        notifyObservers(data);
    }
} // MessageObservable