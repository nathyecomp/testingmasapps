/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pucrio.les.mas.subscriber.framework;

import br.pucrio.les.mas.subscriber.framework.statemachine.StateMachine;
import br.pucrio.les.mas.subscriber.framework.statemachine.StateValues;
import br.pucrio.les.mas.subscriber.framework.statemachine.results.ExceededTimeTest;
import br.pucrio.les.mas.subscriber.framework.statemachine.results.FailedTest;
import br.pucrio.les.mas.subscriber.framework.statemachine.results.SuccessTest;
import br.pucrio.les.mas.subscriber.framework.statemachine.results.WrongTest;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author nathalianascimento
 */
public abstract class RabbitMQConsumer {

    
    private static final String EXCHANGE_NAME = "topic_logs";
        //must be subscribed
    protected abstract void processData(LogContext log);
    public abstract String getBindingKey();
    public abstract String[] getListBindingKey();
    public List<StateMachine> stateMachineList = new ArrayList<>();
    public abstract void createStateMachines();
    

    private void consumeMessageTemplate() throws Exception {
        //String key = getBindingKey();
        String data[] = getListBindingKey(); //{key};
        
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME, "topic");
        String queueName = channel.queueDeclare().getQueue();

        if (data.length < 1) {
            System.err.println("Usage: ReceiveLogsTopic [binding_key]...");
            System.exit(1);
        }

        for (String bindingKey : data) {

            channel.queueBind(queueName, EXCHANGE_NAME, bindingKey);
        }

        System.out.println(" [*] Waiting for messages (logs). To exit press CTRL+C");

        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                    AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body, "UTF-8");
                System.out.println(" [x] Received '" + envelope.getRoutingKey() + "':'" + message + "'");
                
                LogContext logReceived = getLogFromMsg(message);  
                processData(logReceived);
                
            }
        };

        channel.basicConsume(queueName, true, consumer);
    }
    
    private LogContext getLogFromMsg(String msg){
        
        if (msg.length() < 1)
            return null;
        LogContext log = new LogContext();
        String[] messageSplit = msg.split("\\.");
        log.setAgentType(messageSplit[0]);
        log.setAgentName(messageSplit[1]);
        log.setAction(messageSplit[2]);
        log.setTypeLog(messageSplit[3]);
        log.setClassName(messageSplit[4]);
        log.setMethodName(messageSplit[5]);
        log.setCodeLine(messageSplit[6]);
        log.setResource(messageSplit[7]);
        log.setYear(messageSplit[8]);
        log.setMonth(messageSplit[9]);
        log.setDay(messageSplit[10]);
        log.setHour(messageSplit[11]);
        log.setMinute(messageSplit[12]);
        log.setSecond(messageSplit[13]);
        log.setMillisecond(messageSplit[14]);
        String m = "";
        for (int x = 15; x < messageSplit.length; x++) {
            m+= (messageSplit[x]+" ");
        }
        log.setMessage(m);
        
        return log;
    }
    
    public final void execute() {

        try {
            testGui();
            if(this.stateMachineList.isEmpty())
                createStateMachines(); //ele esta retornando e criando novamente..o que interfere nos testes do jUnit - estado atual is first
            consumeMessageTemplate();
           // System.out.println("PSSEI AQUI");
        } catch (Exception ex) {
            Logger.getLogger(RabbitMQConsumer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public abstract void testGui();
    
    StateValues.State previous = null; //tem q ser uma lista de previous 
    public boolean processStateMachines(LogContext log){
        boolean result = false;
        for(StateMachine state: this.stateMachineList){
            if((!state.getCurrent().name().equals(state.getLast().name()))){
             try {
                 StateValues.State currentState = state.getNext(log);
                 if(currentState!=null&&(!currentState.name().equals(state.getLast().name()))){
//                 if(currentState!=null&& currentState!=previous &&
//                         (!currentState.name().equals(state.getLast().name()))){
                     this.previous = currentState;
                     System.out.println("Current state is " + currentState.name());
                 }
             } catch (FailedTest | WrongTest| ExceededTimeTest ex) {
                 System.out.println("Test result is "+ex.getLocalizedMessage());
                 result = false;
                 //this.stateMachineList.remove(state);
                 //System.out.println("Current log is "+log.getLogText());
                 //Logger.getLogger(StreetLightGlobalTest.class.getName()).log(Level.SEVERE, null, ex);
             }
             catch(SuccessTest ex){
                 System.out.println("Test result is "+ex.getLocalizedMessage());
                 result = true;
                 //this.stateMachineList.remove(state);
             }
            }
         }
        return result;
    }

    public List<StateMachine> getStateMachineList() {
        return stateMachineList;
    }
    
}

