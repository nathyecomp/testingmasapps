/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pucrio.les.mas.subscriber.framework;

/**
 *
 * @author nathalianascimento
 */
public interface LogValues {
    interface Action {
        public String name();
    }
    interface TypeLog {
        public String name();
    }
    interface Resource {
        public String name();
    }
    interface MethodName {
        public String name();
    }    
    interface AgentType {
        public String name();
    }
    interface AgentName {
        public String name();
    }
    
}


