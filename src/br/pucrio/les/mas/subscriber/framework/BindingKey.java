/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pucrio.les.mas.subscriber.framework;

/**
 *
 * @author nathalianascimento
 */
public class BindingKey {
    
    String bindingKey="";
    
    public String createBindingKey(String agentTypeValue){
        bindingKey= agentTypeValue+".#";
        return bindingKey;
    }
    
    public String createBindingKey(String agentTypeValue, String agentNameValue){
        bindingKey= agentTypeValue+"."+agentNameValue+".#";
        return bindingKey;
    }
    public String createBindingKey(String agentTypeValue, String agentNameValue, String actionValue){
        bindingKey= agentTypeValue+"."+agentNameValue+"."+actionValue+".#";
        return bindingKey;
    }
    
    public String createBindingKey(LogValues.Action action){
        bindingKey= "*"+"."+"*"+"."+action.name()+".#";
        return bindingKey;
    }
    public String createBindingKey(LogValues.AgentType agentType){
        bindingKey= agentType.name()+"."+"*"+"."+"*"+".#";
        return bindingKey;
    }
    
    public String createBindingKey(LogValues.AgentType agentType, LogValues.Action action){
        bindingKey= agentType.name()+"."+"*"+"."+action.name()+".#";
        return bindingKey;
    }
    
    public String createBindingKey(String agentTypeValue, String agentNameValue, String actionValue,
            String typeLogValue){
        bindingKey= agentTypeValue+"."+agentNameValue+"."+actionValue+"."+typeLogValue+".#";
        return bindingKey;
    }
    
    public String createBindingKey(LogValues.Action action,LogValues.TypeLog typeLog){
        bindingKey= "*"+"."+"*"+"."+action.name()+"."+typeLog.name()+".#";
        return bindingKey;
    }
    
    public String createBindingKey(LogValues.AgentType agentType,LogValues.TypeLog typeLog){
        bindingKey= agentType.name()+"."+"*"+"."+"*"+"."+typeLog.name()+".#";
        return bindingKey;
    }
    public String createBindingKey(LogValues.Action action,LogValues.Resource resourceType){
        bindingKey= "*"+"."+"*"+"."+action.name()+"."+"*"+"."+"*"+"."+"*"+"."+"*"+"."+resourceType.name()+".#";
        return bindingKey;
    }
    
    public String createBindingKey(LogValues.Resource resourceType,LogValues.TypeLog typeLog){
        bindingKey= "*"+"."+"*"+"."+"*"+"."+typeLog.name()+"."+"*"+"."+"*"+"."+"*"+"."+resourceType.name()+".#";
        return bindingKey;
    }
    
    public String createBindingKey(String agentTypeValue, String agentNameValue, String actionValue,
            String typeLogValue, String classNameValue){
        bindingKey= agentTypeValue+"."+agentNameValue+"."+actionValue+"."+typeLogValue+"."+classNameValue+".#";
        return bindingKey;
    }
    public String createBindingKey(String agentTypeValue, String agentNameValue, String actionValue,
            String typeLogValue, String classNameValue, String methodNameValue){
        bindingKey= agentTypeValue+"."+agentNameValue+"."+actionValue+"."+
                typeLogValue+"."+classNameValue+"."+methodNameValue+".#";
        return bindingKey;
    }
    public String createBindingKey(String agentTypeValue, String agentNameValue, String actionValue,
            String typeLogValue, String classNameValue, String methodNameValue, String codeLineValue){
        bindingKey= agentTypeValue+"."+agentNameValue+"."+actionValue+"."+
                typeLogValue+"."+classNameValue+"."+methodNameValue+"."+codeLineValue+".#";
        return bindingKey;
    }
    
   public String createBindingKey(String agentTypeValue, String agentNameValue, String actionValue,
            String typeLogValue, String classNameValue, String methodNameValue, String codeLineValue,
            String resourceValue){
        bindingKey= agentTypeValue+"."+agentNameValue+"."+actionValue+"."+
                typeLogValue+"."+classNameValue+"."+methodNameValue+"."+codeLineValue+"."+resourceValue+".#";
        return bindingKey;
    }
    
    public void printBindingKey(){
        System.out.println("My Binding Key is "+ this.bindingKey);
    }
    
}
