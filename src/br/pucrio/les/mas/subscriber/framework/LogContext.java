/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pucrio.les.mas.subscriber.framework;

/**
 *
 * @author nathalianascimento
 */
public class LogContext {
    String agentType; //
    String agentName; //
    String action;
    String typeLog;
    String className;
    String methodName;
    String codeLine;
    String resource;
    String year;
    String month;
    String day;
    String hour;
    String minute;
    String second;
    String millisecond;
    String message;
    
    public LogContext (){
        
    }
    public LogContext( LogValues.AgentType agentType, LogValues.AgentName agentName, 
            LogValues.Action action, LogValues.Resource resource, 
            LogValues.TypeLog typeLog){
        
        this.agentType = agentType.name();
        this.agentName = agentName.name();
        this.action = action.name();
        this.resource = resource.name();
        this.typeLog = typeLog.name();
    }

    public String getAgentType() {
        return agentType;
    }

    public void setAgentType(String agentType) {
        this.agentType = agentType;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getTypeLog() {
        return typeLog;
    }

    public void setTypeLog(String typeLog) {
        this.typeLog = typeLog;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getCodeLine() {
        return codeLine;
    }

    public void setCodeLine(String codeLine) {
        this.codeLine = codeLine;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getMinute() {
        return minute;
    }

    public void setMinute(String minute) {
        this.minute = minute;
    }

    public String getSecond() {
        return second;
    }

    public void setSecond(String second) {
        this.second = second;
    }

    public String getMillisecond() {
        return millisecond;
    }

    public void setMillisecond(String millisecond) {
        this.millisecond = millisecond;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }
    
    public double getTotalMilliseconds(){
        
        double timeBehavior = 0;
        //convert to millisecond
        double hours = Integer.parseInt(getHour()) * 3600*1000;
        double minutes = Integer.parseInt(getMinute()) * 60 *1000;
        double seconds = Integer.parseInt(getSecond()) * 1000;
        double milliseconds = Integer.parseInt(getMillisecond());
        timeBehavior = hours + minutes + seconds + milliseconds;
        return timeBehavior;
    }
    

    
    public void printLogValues(){
        System.out.println("agentType: "+ this.agentType);
        System.out.println("agentName: "+this.agentName);
        System.out.println("action: "+this.action);
        System.out.println("typeLog: "+ this.typeLog);
        System.out.println("className: "+this.className);
        System.out.println("methodName: "+this.methodName);
        System.out.println("codeLine: "+ this.codeLine);
        System.out.println("resource: "+this.resource);
        System.out.println("year: "+ this.year);
        System.out.println("month: "+this.month);
        System.out.println("day: "+this.day);
        System.out.println("hour: "+ this.hour);
        System.out.println("minute: "+this.minute);
        System.out.println("second: "+ this.second);
        System.out.println("millisecond: "+this.millisecond);
        System.out.println("msg: "+this.message);
        
    }
    
    public boolean compareLogContext(LogContext second){
        return second.getAgentName().equals(this.getAgentName()) &&
                second.getAction().equals((this.getAction()))&&
                second.getAgentType().equals(this.getAgentType()) &&
                second.getResource().equals(this.getResource()) &&
                second.getTypeLog().equals(this.getTypeLog());
    }
    
    public String getLogText(){
        return (this.getAction()+"."+this.getAgentName()+"."+
                this.getResource()+"."+this.getTypeLog());
    }
}
