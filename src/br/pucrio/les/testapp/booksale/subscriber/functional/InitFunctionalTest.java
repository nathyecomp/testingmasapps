/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pucrio.les.testapp.booksale.subscriber.functional;

import br.pucrio.les.mas.subscriber.framework.functional.IntegrationSubscriber;
import br.puc.rio.les.testapp.booksale.subscriber.performance.PerformanceLoadTest;

/**
 *
 * @author nathalianascimento
 */
public class InitFunctionalTest {
    
    public static void main(String[] args) {
        //PerformanceLoadTest testSystem = PerformanceLoadTest.getInstance();
        //testSystem.execute();
       
       IntegrationSubscriber integrationTest = new IntegrationSubscriber();
       integrationTest.execute();
    }
    
}
