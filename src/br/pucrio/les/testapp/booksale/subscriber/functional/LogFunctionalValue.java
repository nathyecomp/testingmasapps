/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pucrio.les.testapp.booksale.subscriber.functional;

import br.pucrio.les.mas.subscriber.framework.LogValues;



/**
 *
 * @author nathalianascimento
 */
public class LogFunctionalValue implements LogValues{
    
   public enum Action implements LogValues.Action {
        connectToSystem,searchBook,receiveListFromDF,isDestroyed,askPrice,
        selectBestPrice, askBuy, receivedSaleConfirmation,receivedBook,
        addBook,receivedBuy,soldBook,removeBook,refuseSell,postedBook,registerService;
    }
    public enum TypeLog implements LogValues.TypeLog{
        INFO, WARNING, ERROR;
    }
  /*  public enum Resource implements LogValues.Resource{
         book,yellowpage,agent,none;
    }
    public enum MethodName implements LogValues.MethodName{
        initAgent,setup,takeDown,buyBook,done,getBookInPostOffice;
    }*/
    public enum AgentType implements LogValues.AgentType{
        CLIENT,SELLER;
    }
   
}
